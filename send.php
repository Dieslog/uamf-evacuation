<?php

$host = 'unicorne.mysql.tools'; //praxis
$user = 'unicorne_praxis';
$password = 'A#9l4n9;rU';
$database = 'unicorne_praxis';
$charset = 'utf8';

$bd = new mysqli($host, $user, $password, $database); //подключение до БД
$bd->set_charset($charset); //кодировка данных
if (mysqli_connect_errno()) {
    exit();
}

$fp = fopen('log.txt', 'a');


function format_number(&$number)
{
    $number = str_replace(array('(', ')', '-', '+'), '', $number);
    if (strlen($number) > 9) {
        $str_phone = substr($number, -10);
        if ($str_phone[0] == '0') {
            $number = '+38' . $str_phone;
        }
    } else
        if (strlen($number) == 9) {
            $str_phone = substr($number, -9);
            $number = '+380' . $str_phone;
        }
}

function dateFormat($date)
{
    return substr($date, 6, 4) . '-' . substr($date, 3, 2) . '-' .
        substr($date, 0, 2);
}

function clone_trainees($phone, $old_format)
{

    $where = '';
    $data = array();

    global $bd;

    if (!empty($phone)) {
        $phone = substr($phone, -9);
    }
    if (!empty($old_format)) {
        $old_format = substr($old_format, -9);
    }

    if (!empty($phone) and !empty($old_format)) {
        $where = "number LIKE '%{$phone}%' OR number LIKE '%{$old_format}%'";
    } else {
        $where = "number LIKE '%{$phone}%'";
    }

    $request = "SELECT id_trainees FROM trainees_phone WHERE {$where}";
    if ($requestResult = $bd->query($request)) {
        while ($result = $requestResult->fetch_assoc()) {
            array_push($data, $result);
        }
    }

    if (count($data)) {
        $array_id = array();
        foreach ($data as $item) {
            $array_id[] = $item['id_trainees'];
        }
        $list_id = implode(',', $array_id);

        $request = "UPDATE trainees SET clone=1 WHERE id_trainees IN ({$list_id})";

        if ($bd->query($request)) {
            return 1;
        } else {
            return 0;
        }
    } else {
        return 0;
    }
}


$place_for_live = '';
if ($_POST['place_for_live']) {
    $place_for_live = "\nПотрібно житло";
}

$who = '';
if ($_POST['who']) {
    $who = "З дітьми\n";
}

$name = addslashes(htmlspecialchars($_POST['name']));
$birth_day = addslashes(htmlspecialchars($_POST['birthday']));
$income_date = date('Y-m-d H:i:s');
$gender = addslashes(htmlspecialchars($_POST['gender']));
$note = "{$who}" . addslashes(htmlspecialchars($_POST['who_message'])) . "{$place_for_live}";
$phone = addslashes(htmlspecialchars($_POST['phone']));
$old_format_phone_number = $phone;
format_number($phone);
$comments = addslashes(htmlspecialchars($_POST['comment']));
$clone = clone_trainees($phone, $old_format_phone_number, $bd);

$col = "id_trainees, snp, birth_day, note, gender, comments, income_date, clone, check_entry, evacuation";
$row = "0, '{$name}', '{$birth_day}', '{$note}', {$gender}, '{$comments}', '{$income_date}', {$clone}, 0, 1";
$request = "INSERT INTO trainees ({$col}) VALUES({$row})";

$first_request = $bd->query($request);

fwrite($fp, json_encode($request) . PHP_EOL);

$id_entry = $bd->insert_id;

$col = "id_phone, number, is_viber, id_trainees";
$row = "0, '{$phone}', 0, {$id_entry}";
$request = "INSERT INTO trainees_phone ({$col}) VALUES({$row})";

$second_request = $bd->query($request);

fwrite($fp, json_encode($request) . PHP_EOL);
fclose($fp);

if ($first_request and $second_request) {
    $bd->close();
    echo json_encode([
        'success' => true
    ]);
} else {
    $bd->close();
    echo json_encode([
        'success' => false
    ]);
}